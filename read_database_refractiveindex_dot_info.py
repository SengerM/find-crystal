import yaml
import numpy as np

def read_file(file):
	with open(file, 'r') as stream:
		content = yaml.load(stream)
	return parse_data(content)

def parse_data(content):
	if content.get('DATA')[0].get('type') == 'tabulated nk':
		return parse_tabulated_nk(content)
	elif content.get('DATA')[0].get('type') == 'tabulated n':
		return parse_tabulated_n(content)
	elif content.get('DATA')[0].get('type') == 'formula 1':
		return parse_formula_1(content)
	elif content.get('DATA')[0].get('type') == 'formula 2':
		return parse_formula_2(content)
	elif content.get('DATA')[0].get('type') == 'formula 3':
		return parse_formula_3(content)
	elif content.get('DATA')[0].get('type') == 'formula 4':
		return parse_formula_4(content)
	elif content.get('DATA')[0].get('type') == 'formula 5':
		return parse_formula_5(content)
	elif content.get('DATA')[0].get('type') == 'formula 6':
		return parse_formula_6(content)
	elif content.get('DATA')[0].get('type') == 'formula 7':
		return parse_formula_7(content)
	else:
		print('Don\'t know how to parse "' + content.get('DATA')[0].get('type') + '" type of data...')
		return None

def parse_tabulated_nk(content):
	wavelength = []
	refractive_index = []
	extinction_coefficient = []
	for row in content['DATA'][0]['data'].split('\n'):
		if row == '':
			continue
		wavelength.append(float(row.split(' ')[0]))
		refractive_index.append(float(row.split(' ')[1]))
		extinction_coefficient.append(float(row.split(' ')[2]))
	return {'lambda': wavelength, 'n': refractive_index, 'k': extinction_coefficient}

def parse_tabulated_n(content):
	wavelength = []
	refractive_index = []
	for row in content['DATA'][0]['data'].split('\n'):
		if row == '':
			continue
		wavelength.append(float(row.split(' ')[0]))
		refractive_index.append(float(row.split(' ')[1]))
	return {'lambda': wavelength, 'n': refractive_index}

def parse_formula_1(content):
	c = c = parse_coefs(content, 17)
	wavelength = get_wavelength(content)
	l = wavelength**2
	n = wavelength*0 + c[0]
	for k in [1,3,5,7,9,11,13,15]:
		n += c[k]*l/(l-c[k+1]**2)
	n += 1
	n = n**.5
	return {'lambda': wavelength, 'n': n}

def parse_formula_2(content):
	c = c = parse_coefs(content, 17)
	wavelength = get_wavelength(content)
	l = wavelength**2
	n = wavelength*0 + c[0]
	for k in [1,3,5,7,9,11,13,15]:
		n += c[k]*l/(l-c[k+1])
	n += 1
	n = n**.5
	return {'lambda': wavelength, 'n': n}

def parse_formula_3(content):
	c = parse_coefs(content, 17)
	wavelength = get_wavelength(content)
	n = wavelength*0 + c[0]
	for k in [1,3,5,7,9,11,13,15]:
		n += c[k]*wavelength**c[k+1]
	n = n**.5
	return {'lambda': wavelength, 'n': n}

def parse_formula_4(content):
	c = parse_coefs(content, 17)
	wavelength = get_wavelength(content)
	n = wavelength*0 + c[0]
	for k in [1, 5]:
		n += c[k]*wavelength**c[k+1]/(wavelength**2 - c[k+2]**c[k+3])
	for k in [9, 11, 13, 15]:
		n += c[k]*wavelength**c[k+1]
	n = n**.5
	return {'lambda': wavelength, 'n': n}

def parse_formula_5(content):
	c = parse_coefs(content, 11)
	wavelength = get_wavelength(content)
	n = wavelength*0 + c[0]
	for k in [1, 3, 5, 7, 9]:
		n += c[k]*wavelength**c[k+1]
	return {'lambda': wavelength, 'n': n}

def parse_formula_6(content):
	c = parse_coefs(content, 11)
	wavelength = get_wavelength(content)
	n = wavelength*0 + c[0]
	for k in [1, 3, 5, 7, 9]:
		n += c[k]/(c[k+1] - wavelength**(-2))
	n += 1
	return {'lambda': wavelength, 'n': n}

def parse_formula_7(content):
	c = parse_coefs(content, 6)
	wavelength = get_wavelength(content)
	n = wavelength*0 + c[0]
	for k in [1, 2]:
		n += c[k]/(wavelength**2 - 0.028)**k
	n += c[3]*wavelength**2 + c[4]*wavelength**4 + c[5]*wavelength**6
	return {'lambda': wavelength, 'n': n}

def parse_formula_9(content):
	# IT IS NOT WORKING OKAY! I get different values than the ones in the web site.
	c = parse_coefs(content, 6)
	wavelength = get_wavelength(content)
	n = c[0] + c[1]/(wavelength**2 - c[2]) + c[3]*(wavelength - c[4])/((wavelength-c[4])**2 + c[5])
	n = n**.5
	return {'lambda': wavelength, 'n': n}

def get_wavelength(content):
	min_wavelength = float(content['DATA'][0]['wavelength_range'].split(' ')[0])
	max_wavelength = float(content['DATA'][0]['wavelength_range'].split(' ')[1])
	wavelength = np.linspace(min_wavelength, max_wavelength) # Wavelength array
	return wavelength

def parse_coefs(content, how_many):
	c = [float(i) for i in content['DATA'][0]['coefficients'].split(' ')]
	for k in range(how_many-len(c)):
		c.append(0)
	return c
