def read_file(file):
	wavelength = []
	nx = []
	ny = []
	nz = []
	with open(file, encoding='iso-8859-14') as f:
		for idx, line in enumerate(f):
			if idx <= 10:
				continue
			things_in_line = line.split(',')
			wavelength.append(float(things_in_line[0] + '.' + things_in_line[1]))
			nx.append(float(things_in_line[2] + '.' + things_in_line[3]))
			ny.append(float(things_in_line[4] + '.' + things_in_line[5]))
			nz.append(float(things_in_line[6] + '.' + things_in_line[7]))
	return {'wavelength': [w*1e-6 for w in wavelength], 'nx': nx, 'ny': ny, 'nz': nz}
