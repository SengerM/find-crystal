import numpy

def costheta_EM(np, n, nprime, alpha):
	return (np**2 + n**2*alpha**2 - nprime**2*(1-alpha)**2)/2/np/n/alpha

def costheta_dEM(np, n, alpha, Epump, Em):
	return (np**2 + n**2*alpha**2 - (1-alpha)**2 + Em**2/Epump**2)/2/np/n/alpha
