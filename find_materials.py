RESULTS_DIR_PATH = '/home/alf/Escritorio/dark_photon_material_search_results'
REFRACTIVEINDEX_DOT_INFO_DATABASE_REPO_PATH = '/home/alf/Escritorio/refractiveindex.info-database/'
PHASEMATCHING4PC_DATABASE_DIR_PATH = '/home/alf/Escritorio/phasematch4pc/exported_data/'
SIGNAL_WAVELENGTH_SEARCH_RANGE = [250e-9, 700e-9] # Meters
ALPHA = .4
DARK_PHOTON_MASS_IN_eV = 0 # eV
REQUIRE_CLASSICAL_SPDC = True # True or False
MAXIMUM_MINIMUM_K_IN_SIGNAL_SEARCH_RANGE = .00001 # Maximum value allowed for the minimum of the extinction coef in the signal wavelength search range

########################################################################

PLANK_CONSTANT = 6.62607015e-34 # J*s
SPEED_OF_LIGHT = 299792458 # m/s
eV_OVER_JOULE = 6.242e+18 # eV/Joule

########################################################################

if DARK_PHOTON_MASS_IN_eV > PLANK_CONSTANT*SPEED_OF_LIGHT/min(SIGNAL_WAVELENGTH_SEARCH_RANGE)*eV_OVER_JOULE:
	raise ValueError('You cannot look for a particle with a mass of ' + str(DARK_PHOTON_MASS_IN_eV) + ' eV using pump photons of ' + str(PLANK_CONSTANT*SPEED_OF_LIGHT/min(SIGNAL_WAVELENGTH_SEARCH_RANGE)/ALPHA*eV_OVER_JOULE) + ' eV and an alpha coef of ' + str(ALPHA) + ' dude...')

from read_database_refractiveindex_dot_info import read_file as ridi_read
from read_phasematch4pc_database import read_file as pm4pc_read
import matplotlib.pyplot as plt
import os
import numpy as np
from EMdEM import *

def correct_path(path):
	return path if path[-1] == '/' else path + '/'

def check_wavelength_range(wavelength_data):
	signal_search_range_data_indices = []
	for idx, w in enumerate(wavelength_data):
		if w > min(SIGNAL_WAVELENGTH_SEARCH_RANGE) and w < max(SIGNAL_WAVELENGTH_SEARCH_RANGE):
			signal_search_range_data_indices.append(idx)
	return signal_search_range_data_indices

RESULTS_DIR_PATH = correct_path(RESULTS_DIR_PATH)
REFRACTIVEINDEX_DOT_INFO_DATABASE_REPO_PATH = correct_path(REFRACTIVEINDEX_DOT_INFO_DATABASE_REPO_PATH)
PHASEMATCHING4PC_DATABASE_DIR_PATH = correct_path(PHASEMATCHING4PC_DATABASE_DIR_PATH)

if RESULTS_DIR_PATH.split('/')[-2] not in os.listdir( RESULTS_DIR_PATH[:RESULTS_DIR_PATH.rfind('/', 0, len(RESULTS_DIR_PATH)-1)] ):
	os.mkdir(RESULTS_DIR_PATH)
if 'plots' not in os.listdir(RESULTS_DIR_PATH):
	os.mkdir(RESULTS_DIR_PATH + 'plots')
os.system('rm ' + RESULTS_DIR_PATH + 'plots/*')
	
with open(RESULTS_DIR_PATH + 'materials_for_dark_photon_production.md', 'w') as f:
	print('# Search of materials for dark SPDC', file=f)
	print('', file=f)
	print('- Database source: https://refractiveindex.info/', file=f)
	print('- Signal wavelength search range: ' + str(min(SIGNAL_WAVELENGTH_SEARCH_RANGE)*1e9) + ' to ' + str(max(SIGNAL_WAVELENGTH_SEARCH_RANGE)*1e9) + ' nm', file=f)
	print('- Value of *alpha*: ' + str(ALPHA), file=f)
	print('\t- For this alpha the *pump wavelength* falls between ' + str(min(SIGNAL_WAVELENGTH_SEARCH_RANGE)*ALPHA*1e9) + ' and ' + str(max(SIGNAL_WAVELENGTH_SEARCH_RANGE)*ALPHA*1e9) + ' nm ({:.2f} and {:.2f} eV)'.format(PLANK_CONSTANT*SPEED_OF_LIGHT/min(SIGNAL_WAVELENGTH_SEARCH_RANGE)/ALPHA*eV_OVER_JOULE, PLANK_CONSTANT*SPEED_OF_LIGHT/max(SIGNAL_WAVELENGTH_SEARCH_RANGE)/ALPHA*eV_OVER_JOULE), file=f)
	print('- Dark photon assumed mass: ' + str(DARK_PHOTON_MASS_IN_eV) + ' eV', file=f)
	print('', file=f)
	print('- Maximum value allowed for the minimum value of *k* in the signal search range: ' + str(MAXIMUM_MINIMUM_K_IN_SIGNAL_SEARCH_RANGE), file=f)
	print('', file=f)
	print('## Materials found', file=f)
	print('', file=f)
	print('Below there is a list of materials that satisfy the phase matching conditions for a photon decaying into a signal photon and an idler dark photon.', file=f)
	print('', file=f)
	
files = [PHASEMATCHING4PC_DATABASE_DIR_PATH + file for file in os.listdir(PHASEMATCHING4PC_DATABASE_DIR_PATH)] # + [os.path.join(dp, f) for dp, dn, fn in os.walk(os.path.expanduser(REFRACTIVEINDEX_DOT_INFO_DATABASE_REPO_PATH + 'database/data/')) for f in fn]

succes = 0
for file in files:
	if 'refractiveindex.info-database' in file:
		if ridi_read(file) is None: # We don't know how to read this file...
			continue
		data = ridi_read(file)
		wavelength = [w*1e-6 for w in data.get('lambda')] # Convert it to meters.
		refractive_index = data.get('n')
		extinction_coeff = data.get('k')
		
		signal_search_range_data_indices = check_wavelength_range(wavelength)
		
		if len(signal_search_range_data_indices) == 0: # There is no data in our range of interest.
			continue
		
		if extinction_coeff is not None:
			if min([extinction_coeff[idx] for idx in signal_search_range_data_indices]) > MAXIMUM_MINIMUM_K_IN_SIGNAL_SEARCH_RANGE: # The "k" is greater than our maximum value allowed in all the range. Discard material.
				continue
		
		for idx_pump, wavelength_pump in enumerate(wavelength):
			n_pump = refractive_index[idx_pump]
			wavelength_signal = wavelength_pump/ALPHA
			if True not in (wavelength_signal<np.array(wavelength[idx_pump:])): # This means that we have no data for wavelengths of interest.
				continue
			idx_signal_best_fit = np.argmin( [(l-wavelength_signal)**2 for l in wavelength] ) # Find the index at which the data wavelength best fit the signal wavelength.
			n_signal = refractive_index[idx_signal_best_fit] # Get the refraction index as that associated with the wavelength that best match.
			if wavelength_signal < min(SIGNAL_WAVELENGTH_SEARCH_RANGE) or wavelength_signal > max(SIGNAL_WAVELENGTH_SEARCH_RANGE): # Discard this result, we are not interested.
				continue
			costeta_dEM = costheta_dEM(n_pump, n_signal, ALPHA, PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_pump*eV_OVER_JOULE, DARK_PHOTON_MASS_IN_eV)
			if not (costeta_dEM < 1 and costeta_dEM > -1): # This means that this material does NOT satisfy the dEM phase matching condition.
				continue
			if REQUIRE_CLASSICAL_SPDC is True:
				wavelength_idler = wavelength_pump*wavelength_signal/(wavelength_signal-wavelength_pump)
				if wavelength_idler < min(wavelength) or wavelength_idler > max(wavelength): # This means that we have no data for the idler photon.
					continue
				idx_idler_best_fit = np.argmin( [(l-wavelength_idler)**2 for l in wavelength] ) # Find the index at which the data wavelength best fit the idler wavelength.
				n_idler = refractive_index[idx_idler_best_fit] # Get the refraction index as that associated with the wavelength that best match.
				costeta_EM = costheta_EM(n_pump, n_signal, n_idler, ALPHA)
				if not (costeta_EM < 1 and costeta_EM > -1): # This means that this material does NOT satisfy the classical SPDC phase matching condition.
					continue
			# If we are here, it means that we have found a material!
			succes += 1
			with open(RESULTS_DIR_PATH + 'materials_for_dark_photon_production.md', 'a') as f:
				material_path_in_database = file.replace(REFRACTIVEINDEX_DOT_INFO_DATABASE_REPO_PATH, '')
				web_link = 'https://refractiveindex.info/?shelf=' + (material_path_in_database.split('/')[2] + '&book=' + material_path_in_database.split('/')[4] + '&page=' + material_path_in_database.split('/')[5][:-4] if len(material_path_in_database.split('/')) == 6 else material_path_in_database.split('/')[1] + '&book=' + material_path_in_database.split('/')[3] + '&page=' + material_path_in_database.split('/')[4][:-4])
				print(str(succes) + '. ' + REFRACTIVEINDEX_DOT_INFO_DATABASE_REPO_PATH.split('/')[-2] + '/' + material_path_in_database, file=f)
				print('\t- [Link to "refractiveindex.info" web site](' + web_link + ')', file=f)
				print('\t- pump = {:.2f} nm ({:.2f} eV)'.format(wavelength_pump*1e9, PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_pump*eV_OVER_JOULE), file=f)
				print('\t- signal = {:.2f} nm ({:.2f} eV)'.format(wavelength_signal*1e9, PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_signal*eV_OVER_JOULE), file=f)
				print('\t- theta_dark = {:.2f}'.format(np.arccos(costeta_dEM)*180/np.pi) + ' deg', file=f)
				if REQUIRE_CLASSICAL_SPDC is True:
					print('\t- theta_SPDC = {:.2f}'.format(np.arccos(costeta_EM)*180/np.pi) + ' deg', file=f)
			fig, ax = plt.subplots()
			fig.suptitle(material_path_in_database)
			ax.plot([w*1e9 for w in wavelength], refractive_index, label='n')
			ax.plot( [wavelength_pump*1e9 for i in [1,2]], [0, n_pump],	color = (.6,.6,.6) )
			ax.text( wavelength_pump*1e9, n_pump, s = 'pump', rotation = 90, rotation_mode = 'anchor', horizontalalignment = 'right' )
			ax.plot( [wavelength_signal*1e9 for i in [1,2]], [0, n_signal], color = (.6,.6,.6) )
			ax.text( wavelength_signal*1e9, n_signal, s = 'dark signal', rotation = 90, rotation_mode = 'anchor', horizontalalignment = 'right' )
			ax.fill_between( [wavelength[idx]*1e9 for idx in signal_search_range_data_indices], [refractive_index[idx] for idx in signal_search_range_data_indices], alpha = .2, label = 'Signal search range' )
			if REQUIRE_CLASSICAL_SPDC is True:
				ax.plot( [wavelength_idler*1e9 for i in [1,2]], [0, n_idler], color = (.6,.6,.6) )
				ax.text( wavelength_idler*1e9, n_idler, s = 'SPDC idler', rotation = 90, rotation_mode = 'anchor', horizontalalignment = 'right' )
			if data.get('k') is not None: ax.plot([w*1e9 for w in wavelength], extinction_coeff, label='k')
			ax.grid(b=True, which='both', color=(.8,.8,.8))
			ax.legend()
			ax.set_xlabel('Wavelength (nm)')
			if max(wavelength)/min(wavelength) > 10: ax.set_xscale('log')
			if max(refractive_index)/min(refractive_index) > 10: ax.set_yscale('log')
			fig.savefig(RESULTS_DIR_PATH + 'plots/' + str(succes) + '.png')
			plt.close(fig) # This is for not overloading the RAM memory.
			break # If we found just one case in which the current material can produce dark SPDC, we continue with other materials. We are not looking for the best, just looking for those that can produce dark SPDC.
	
	if 'phasematch4pc' in file:
		data = pm4pc_read(file)
		wavelength = data.get('wavelength')
		nx = data.get('nx')
		ny = data.get('ny')
		nz = data.get('nz')
		
		fig, ax = plt.subplots()
		ax.plot([w*1e9 for w in wavelength], nx, label = r'$n_x$')
		ax.plot([w*1e9 for w in wavelength], ny, label = r'$n_y$')
		ax.plot([w*1e9 for w in wavelength], nz, label = r'$n_z$')
		ax.set_xlabel('Wavelength (nm)')
		ax.legend()
		ax.set_xscale('log')
		fig.suptitle(file.split('/')[-1])
		fig.savefig(file.split('/')[-1] + '.png')
		
		signal_search_range_data_indices = check_wavelength_range(wavelength)
		
		if len(signal_search_range_data_indices) == 0: # There is no data in our range of interest.
			continue
		
		for idx_pump, wavelength_pump in enumerate(wavelength):
			wavelength_signal = wavelength_pump/ALPHA
			if True not in (wavelength_signal<np.array(wavelength[idx_pump:])): # This means that we have no data for wavelengths of interest.
				continue
			idx_signal_best_fit = np.argmin( [(l-wavelength_signal)**2 for l in wavelength] ) # Find the index at which the data wavelength best fit the signal wavelength.
			if wavelength_signal < min(SIGNAL_WAVELENGTH_SEARCH_RANGE) or wavelength_signal > max(SIGNAL_WAVELENGTH_SEARCH_RANGE): # Discard this result, we are not interested.
				continue
			wavelength_idler = wavelength_pump*wavelength_signal/(wavelength_signal-wavelength_pump)
			idx_idler_best_fit = np.argmin( [(l-wavelength_idler)**2 for l in wavelength] ) # Find the index at which the data wavelength best fit the idler wavelength.
			
			px_sy_iz_costeta_dark = costheta_dEM(
				np = nx[idx_pump],
				n = ny[idx_signal_best_fit],
				alpha = ALPHA, 
				Epump = PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_pump*eV_OVER_JOULE, 
				Em = DARK_PHOTON_MASS_IN_eV)
			py_sx_iz_costeta_dark = costheta_dEM(
				np = ny[idx_pump],
				n = nx[idx_signal_best_fit],
				alpha = ALPHA, 
				Epump = PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_pump*eV_OVER_JOULE, 
				Em = DARK_PHOTON_MASS_IN_eV)
			pz_sy_ix_costeta_dark = costheta_dEM(
				np = nz[idx_pump],
				n = ny[idx_signal_best_fit],
				alpha = ALPHA, 
				Epump = PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_pump*eV_OVER_JOULE, 
				Em = DARK_PHOTON_MASS_IN_eV)
			px_sz_iy_costeta_dark = costheta_dEM(
				np = nx[idx_pump],
				n = nz[idx_signal_best_fit],
				alpha = ALPHA, 
				Epump = PLANK_CONSTANT*SPEED_OF_LIGHT/wavelength_pump*eV_OVER_JOULE, 
				Em = DARK_PHOTON_MASS_IN_eV)
			dark_costetas = [px_sy_iz_costeta_dark, py_sx_iz_costeta_dark, pz_sy_ix_costeta_dark, px_sz_iy_costeta_dark] 
			
			if REQUIRE_CLASSICAL_SPDC is True:
				px_sy_iz_costeta_EM = costheta_EM(
					np = nx[idx_pump], 
					n = ny[idx_signal_best_fit], 
					nprime = nz[idx_idler_best_fit],
					alpha = ALPHA)
				py_sx_iz_costeta_EM = costheta_EM(
					np = ny[idx_pump], 
					n = nx[idx_signal_best_fit], 
					nprime = nz[idx_idler_best_fit],
					alpha = ALPHA)
				pz_sy_ix_costeta_EM = costheta_EM(
					np = nz[idx_pump], 
					n = ny[idx_signal_best_fit], 
					nprime = nx[idx_idler_best_fit],
					alpha = ALPHA)
				px_sz_iy_costeta_EM = costheta_EM(
					np = nx[idx_pump], 
					n = nz[idx_signal_best_fit], 
					nprime = ny[idx_idler_best_fit],
					alpha = ALPHA)
				EM_costetas = [px_sy_iz_costeta_EM, py_sx_iz_costeta_EM, pz_sy_ix_costeta_EM, px_sz_iy_costeta_EM]
			success_dark = []
			success_EM = []
			for idx in range(len(dark_costetas)):
				if dark_costetas[idx] > -1 and dark_costetas[idx] < 1:
					success_dark.append(idx)
				if REQUIRE_CLASSICAL_SPDC is True:
					if EM_costetas[idx] > -1 and EM_costetas[idx] < 1:
						success_EM.append(idx)

with open(RESULTS_DIR_PATH + 'materials_for_dark_photon_production.md', 'a') as f:
	print('', file=f)
	print('---', file=f)
	print('', file=f)
	print('Found ' + str(succes) + ' materials in a total of ' + str(len(files)) + ' in the database.', file=f)
